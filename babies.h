/*  Defines */
#define MAXLENGTH 20
#define ROWS 200

/* Struct definitions */
struct pNames {
   int  year;
   int  rank[ROWS];
   char maleName[ROWS][MAXLENGTH];   
   int  maleNumber[ROWS];
   char femaleName[ROWS][MAXLENGTH];   
   int  femaleNumber[ROWS];
};

char* removeCommas (char *line){
	int character;
	int sizeOfLine = strlen(line);
	int convertCount = 0;
	int i = 0;
      for (i = 0; i < sizeOfLine; i = i + 1){
         character = line[i];
         if (character != 44){
               line[i-convertCount] = line[i];
         }else{
               convertCount++;
         }
      }
    line[sizeOfLine-convertCount] = 0;
    return (line);
}
