#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "babies.h"
int main ( int argc, char *argv[] ) {

   FILE *f1;
   int i = 0;
   int j = 0;
   struct pNames popNames;
   char maleSNumber[20];
   char femaleSNumber[20];
   char string[100];
   /*for ( j=0; j<i; j++ ) {*/
   /*   printf ( "%d %s %d %s %d\n", popNames.rank[j], popNames.maleName[j], popNames.maleNumber[j], 
                                                     popNames.femaleName[j], popNames.femaleNumber[j] );
   }*/
   int decadeOne;
   int decadeTwo;
   char fileOne[100];
   char fileTwo[100];
   printf("What 2 decades do you want to look at?\n");
   scanf("%d %d",&decadeOne, &decadeTwo);
   if (decadeOne == 1880){
      strcpy(fileOne,"1880Names.txt");
   }else if(decadeOne == 1890){
      strcpy(fileOne,"1890Names.txt");
   }else if(decadeOne == 1900){
      strcpy(fileOne,"1900Names.txt");
   }else if(decadeOne == 1910){
      strcpy(fileOne,"1910Names.txt");
   }else if(decadeOne == 1920){
      strcpy(fileOne,"1920Names.txt");
   }else if(decadeOne == 1930){
      strcpy(fileOne,"1930Names.txt");
   }else if(decadeOne == 1940){
      strcpy(fileOne,"1940Names.txt");
   }else if(decadeOne == 1950){
      strcpy(fileOne,"1950Names.txt");
   }else if(decadeOne == 1960){
      strcpy(fileOne,"1960Names.txt");
   }else if(decadeOne == 1970){
      strcpy(fileOne,"1970Names.txt");
   }else if(decadeOne == 1980){
      strcpy(fileOne,"1980Names.txt");
   }else if(decadeOne == 1990){
      strcpy(fileOne,"1990Names.txt");
   }else if(decadeOne == 2000){
      strcpy(fileOne,"2000Names.txt");
   }else if(decadeOne == 2010){
      strcpy(fileOne,"2010Names.txt");
   }else{
      printf("Terminating program, invalid decade for first");
      return (0);
   }
   if (decadeTwo == 1880){
      strcpy(fileTwo,"1880Names.txt");
   }else if(decadeTwo == 1890){
      strcpy(fileTwo,"1890Names.txt");
   }else if(decadeTwo == 1900){
      strcpy(fileTwo,"1900Names.txt");
   }else if(decadeTwo == 1910){
      strcpy(fileTwo,"1910Names.txt");
   }else if(decadeTwo == 1920){
      strcpy(fileTwo,"1920Names.txt");
   }else if(decadeTwo == 1930){
      strcpy(fileTwo,"1930Names.txt");
   }else if(decadeTwo == 1940){
      strcpy(fileTwo,"1940Names.txt");
   }else if(decadeTwo == 1950){
      strcpy(fileTwo,"1950Names.txt");
   }else if(decadeTwo == 1960){
      strcpy(fileTwo,"1960Names.txt");
   }else if(decadeTwo == 1970){
      strcpy(fileTwo,"1970Names.txt");
   }else if(decadeTwo == 1980){
      strcpy(fileTwo,"1980Names.txt");
   }else if(decadeTwo == 1990){
      strcpy(fileTwo,"1990Names.txt");
   }else if(decadeTwo == 2000){
      strcpy(fileTwo,"2000Names.txt");
   }else if(decadeTwo == 2010){
      strcpy(fileTwo,"2010Names.txt");
   }else{
      printf("Terminating program, invalid decade for first");
      return (0);
   }
      if ( (f1 = fopen(fileOne[1], "r")) != NULL ) {
      while ( fgets(string, 100, f1) != NULL ) {
         sscanf (string, "%d %s %s %s %s", &popNames.rank[i], popNames.maleName[i], maleSNumber, popNames.femaleName[i], femaleSNumber);
         removeCommas( maleSNumber );
         removeCommas( femaleSNumber );
         popNames.maleNumber[i] = atoi ( maleSNumber );
         popNames.femaleNumber[i] = atoi ( femaleSNumber );
         i++;
      }
   } else {
      printf ( "Cannot open %s\n", argv[1] );
      return ( -2 );
   }
      if ( (f1 = fopen(fileTwo[1], "r")) != NULL ) {
      while ( fgets(string, 100, f1) != NULL ) {
         sscanf (string, "%d %s %s %s %s", &popNames.rank[i], popNames.maleName[i], maleSNumber, popNames.femaleName[i], femaleSNumber);
         removeCommas( maleSNumber );
         removeCommas( femaleSNumber );
         popNames.maleNumber[i] = atoi ( maleSNumber );
         popNames.femaleNumber[i] = atoi ( femaleSNumber );
         i++;
      }
   } else {
      printf ( "Cannot open %s\n", argv[1] );
      return ( -2 );
   }
   if (decadeOne == decadeTwo){
      printf("Terminating program, decades cannot match!");
      return (0);
   }
   printf("Would you like to see a rank, search for a name, or see the top 10?");
   printf("What rank do you wish to see? [rank, search, top]");
   printf("Would you like to see the male (0), female (1), or both (2) name(s)? [0-2]");
   printf("What name do you want to search for?");
   printf("Do you wish to search male (0), female (1), or both (2) name? [0-2]");
   printf("Do you want to ask another question about 1880 and 1980? [Y or N]");
   printf("Would you like to select other decades? [Y or N]");
   return (0);
}
